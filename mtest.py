import itertools
from datetime import date


def get_earliest_date(possible_dates, min_date, max_date):
    dates = []

    for d in possible_dates:
        try:
            dates.append(date(d[0], d[1], d[2]))
        except ValueError:
            pass
    dates_in_range = [d for d in dates if min_date < d < max_date]
    if not dates_in_range:
        return 'is illegal'
    return min (dates_in_range)

def earliest_possible_date(line, min_date, max_date):
    seq = map(int, line.split('/'))

    if max(seq) > 99:
        year = seq.pop(seq.index(max(seq)))
        possible_dates = [[year, i[0], i[1]] for i in set(itertools.permutations(seq))]
        print get_earliest_date(possible_dates, min_date, max_date)
    else:
        possible_dates = [[i[0] + 2000, i[1], i[2]] for i in set(itertools.permutations(seq))]
        print get_earliest_date(possible_dates, min_date, max_date)

if __name__ == '__main__':

    with open('f.txt', 'r') as f:
        line = f.read()

    earliest_possible_date(line, date(2000,1,1), date(2999,12,31))
